import logo from './logo.svg';
import React from 'react';
import './App.css';
import ToDoItem from './toDo/ToDoItem'
import todosData from './toDo/todosData'

function App() {
  const todosItem = todosData.map(item => {
    return (
      <ToDoItem
      key = {item.id}
      description ={item.text}
      complited = {item.complited}
      />
    );
  })

  const inputRef = React.createRef();

  const getValue = () => {
    console.log(inputRef.current.value);
  }
  return (
    <div className="App ">
        {todosItem}
        <input ref={inputRef} />
        <button onClick = {getValue}> push me</button>
    </div>
  )
}

export default App;
