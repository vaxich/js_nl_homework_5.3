import React from 'react';
import "./ToDoItem.scss";

function ToDoItem(props) {
  
    return (
        <div className="todo-item">
            <input  type="checkbox" defaultChecked={props.complited} />
            <p className = "description">{props.description}</p>
            
        </div>
    );
}

export default ToDoItem;