

const todosData = [
    {
        id: 1,
        text: "Сходить на работу",
        complited: true
    },
    {
        id: 2,
        text: "Почистить зубы",
        complited: false
    },
    {
        id: 3,
        text: "Сварить борщ",
        complited: true
    },
    {
        id: 4,
        text: "Запривить Машину",
        complited: true
    },
    {
        id: 5,
        text: "Купить молоко",
        complited: false
    },
    {
        id: 6,
        text: "Погулять с собакой",
        complited: true
    }
]

export default todosData;